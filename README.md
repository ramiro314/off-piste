# Gnar Off-Piste

[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)
[![codecov](https://codecov.io/gl/gnaar/off-piste/branch/master/graph/badge.svg?token=MRowdXaujg)](https://codecov.io/gl/gnaar/off-piste)
[![pipeline status](https://gitlab.com/gnaar/off-piste/badges/master/pipeline.svg)](https://gitlab.com/gnaar/off-piste/commits/master)

**Part of Project Gnar:** &nbsp;[base](https://hub.docker.com/r/gnar/base) &nbsp;•&nbsp; [gear](https://pypi.org/project/gnar-gear) &nbsp;•&nbsp; [piste](https://gitlab.com/gnaar/piste) &nbsp;•&nbsp; [off-piste](https://gitlab.com/gnaar/off-piste) &nbsp;•&nbsp; [edge](https://www.npmjs.com/package/gnar-edge) &nbsp;•&nbsp; [powder](https://gitlab.com/gnaar/powder)  &nbsp;•&nbsp; [patrol](https://gitlab.com/gnaar/patrol)

Project Gnar is a full stack, turnkey starter web app which includes:

- Sign up email with secure account activation
- Secure login and session management via JWT
- Basic account details (name, address, etc.)
- Password reset email with secure confirmation
- React-based frontend and Python-based microservice backend
- Microservice intra-communication
- SQS message polling and sending
- AWS Cloud-based hosting and Kubernetes deployment

A detailed, step-by-step setup walkthrough will be published on [Medium](https://medium.com) in September 2018 - check back here for the link.

A demo site is up at [gnar.ca](https://app.gnar.ca) - everyone is welcome to create an account and test the workflows.

Gnar Off-Piste is the ancillary Python microservice of Project Gnar. It's built with [Gnar Gear](https://pypi.org/project/gnar-gear) and is designed to showcase Gear's microservice intra-communication and SQS message handling.

In development, start Off-Piste with

```bash
$ python3 app/main.py -p 9401
```

The development server is a fault-tolerant version of [Flask's WSGI Development Server](http://flask.pocoo.org/docs/1.0/server), provided by [Gnar Gear](https://pypi.org/project/gnar-gear).

For production, build a [Gnar Base](https://hub.docker.com/r/gnar/base)-based Docker image using Off-Piste's [Dockerfile](https://gitlab.com/gnaar/off-piste/blob/master/Dockerfile) and deploy it to a Kubernetes cluster with [Gnar Patrol](https://gitlab.com/gnaar/patrol)'s [off-piste-deployment.yml](https://gitlab.com/gnaar/patrol/blob/master/manifest/off-piste-deployment.yml).

```bash
$ docker build -t gnar/off-piste:latest .  # Replace 'gnar' with your Docker / ECR account name
```

The production server is a [Bjoern WSGI Production Server](https://github.com/jonashaag/bjoern), also provided by [Gnar Gear](https://pypi.org/project/gnar-gear).

On startup, Gnar Gear configures the following services:
- Logger & error handler
- Flask blueprints (auto-registered)

Off-Piste uses Gnar Gear's SQS message sender which is dependent on the following environment variables:

```bash
GNAR_SQS_ACCESS_KEY_ID
GNAR_SQS_REGION_NAME
GNAR_SQS_SECRET_ACCESS_KEY
```

See the [Gnar Gear docs](https://pypi.org/project/gnar-gear) for details on these environment variables as well as other, optional, envars.

### Blueprints

- **/check-in** GET: Receives a request from [Piste](https://gitlab.com/gnaar/piste), sends an SQS message and responds to Piste with the SQS message id

---

<p><div align="center">Made with <img alt='Love' width='32' height='27' src='https://s3-us-west-2.amazonaws.com/project-gnar/heart-32x27.png'> by <a href='https://www.linkedin.com/in/briengivens'>Brien Givens</a></div></p>
